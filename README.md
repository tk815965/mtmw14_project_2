# MTMW14_Project_2
- plans.pdf contains images of project plans
- MTMW14_Proiect_2.ipynb contains the jupyter notebook for the project
- MTMW14_Project_2_29815965.pdf contains the final project report

The following files must be downloaded in order to run the project:

- parameters.py contains parameters, dimensions, initial conditions, wind stress function for the project
- plots.py contains functions for all the plots produced
- solution.py contains the function for the analytical solution
- grid.py contains the Arakawa-C grid dimensions
- schemes contians the Forward-Backward scheme 
- energy.py contains the energy functions
- Semi_lagrangian contians the Semi_lagrangian scheme


# Run the main.py file to produce all the plots for Project 2
