from schemes import forward_backward as fb
import numpy as np
from parameters import wind_stress
from energy import energy_fn
from scipy.interpolate import RegularGridInterpolator

def semi_lagrangian(grid, initial, dimensions, setup, nt, analytic_solution):
    """
    Semi-Lagrangian numerical scheme to solve the SWEs. Calculate the perturbations
    and energy at each time point.
    Inputs:  - grid, finite difference grid
             - initial, initial conditions
             - dimensions, spatial and temporal dimensions of x,y and t
             - setup, dictionary of parameters
             - nt, number of time steps
            - analytic_solution, the analytical solution of the SWEs
    Outputs: - u, horizontal velocity after nt time steps
             - v, vertical velocity after nt time steps
             - eta, elevation after nt time steps
             - t, time array
             - energy_arr, array of energy at each time point
             - final_uper, u perturbation from analytic at final time
             - final_vper, v perturbation from analytic at final time
             - final_etaper, eta perturbation from analytic at final time
             - energy_arr_p, energy of perturbation at each time
    """
    # a lot of the functions are copied from Forward_Backward scheme, could be more efficient
    # if the functions were not inside that function, so they could be imported
    # another improvement might be to work out everything from the beginning on the eta grid?

    # get parameters
    L, f_0, beta, g, gamma, rho, H, tau_0 = setup['L'], setup['f_0'], setup['beta'], \
                                            setup['g'], setup['gamma'], setup['rho'], setup['H'], setup['tau_0']

    # set dimensions
    dx, dy, nx, ny, dt = dimensions['dx'], dimensions['dy'], dimensions['nx'], dimensions['ny'], \
                         dimensions['dt']

    # set grid spacing
    u, v, eta = grid(nx, ny)

    # set initial values
    u_0, v_0, eta_0 = initial['u'], initial['v'], initial['eta']
    u.fill(u_0)
    v.fill(v_0)
    eta.fill(eta_0)

    # values of x and y on the eta grid
    x_arr = np.array(np.arange(0.5, (nx * dx) + 0.5, dx))
    y_arr = np.array(np.arange(0.5, (ny * dy) + 0.5, dy))
    # the grids of x and y
    x_mat, y_mat = np.meshgrid(x_arr, y_arr)


    # y for the coriolis terms
    y_arrf = np.array([np.arange(0, ny * dy, dy)])
    y_matf = np.tile(y_arrf.T, nx)

    # get wind stress
    t_x, t_y = wind_stress(tau_0, y_arrf, L)
    tau_x, tau_y = np.array(t_x), np.array(t_y)

    # create u and v with no boundaries
    u_nb, v_nb = np.empty([ny, nx - 1]), np.empty([ny - 1, nx])

    # functions for interpolation
    def dudx(u):
        du = np.diff(u, axis=1)  # size will be (ny, nx)
        return du / dx
    def dvdy(v):
        dv = np.diff(v, axis=0)  # size will be (ny, nx)
        return dv / dy
    def dndx(eta):
        dn = np.diff(eta, axis=1)  # size will be (ny, nx-1)
        return dn / dx
    def dndy(eta):
        dn = np.diff(eta, axis=0)  # size will be (ny-1, nx)
        return dn / dy

    # functions to calculate the interpolated coriolis term
    def cor_v(f_0, b, y, v):
        frac_v = v[:-1, :-1] + v[1:, :-1] + v[:-1, 1:] + v[1:, 1:]
        cor = f_0 + b * (y[:, 1:] - 0.5 * dy)  # offset y as it is not on the same grid as u
        return frac_v * cor * 0.25

    def cor_u(f_0, b, y, u):
        frac_u = u[:-1, :-1] + u[1:, :-1] + u[:-1, 1:] + u[1:, 1:]
        cor = f_0 + b * (y[1:, :])
        return frac_u * cor * 0.25

    # function to calculate u, v and eta at time n+1
    def eta_grid(u, v, eta):
        div = dudx(u) + dvdy(v)
        return eta - (H * dt * div)

    def u_grid(u, v, eta):
        u_new = u.copy()
        m, n = np.shape(np.empty_like(u_nb))
        wind = np.tile(tau_x.T, n)
        result = cor_v(f_0, beta, y_matf, v) - g * dndx(eta) - gamma * u[:, 1:-1] + wind / (rho * H)
        u_new[:, 1:-1] = u[:, 1:-1] + dt * result
        return u_new

    def v_grid(u, v, eta):
        v_new = v.copy()
        # no wind as tau_y is zero
        result = -cor_u(f_0, beta, y_matf, u) - g * dndy(eta) - gamma * v[1:-1, :]
        v_new[1:-1, :] = v[1:-1, :] + dt * result
        return v_new

    u_new, v_new, eta_new = u.copy(), v.copy(), eta.copy()

    # time array
    t = np.arange(0, nt * dt, dt)

    # array containing energy at all times
    energy_arr = np.empty(nt)
    energy_arr_p = np.empty_like(energy_arr)

    u_ana, v_ana, eta_ana = analytic_solution

    # function to interpolate everything to the eta grid
    def u_to_eta(x):
        x_int = 0.5 * (x[:, :-1] + x[:, 1:])
        return x_int

    def v_to_eta(x):
        x_int = 0.5 * (x[:-1, :] + x[1:, :])
        return x_int

    # functions to interpolate back to the u and v grids
    def eta_to_u(x):
        u_grid = np.zeros_like(u)
        int = 0.5 * (x[:, :-1] + x[:, 1:])
        u_grid[:, 1:-1] = int
        return u_grid

    def eta_to_v(x):
        v_grid = np.zeros_like(v)
        v_grid[1:-1,: ] = 0.5 * (x[:-1, :] + x[1:, :])
        return v_grid

    for it in range(nt):
        u_previous, v_previous, eta_previous = u, v, eta
        # have the source terms (S) and enforce BCs
        eta_source = eta_grid(u_previous, v_previous, eta_previous)
        if it % 2 == 0:
            u_source = u_grid(u_previous, v_previous, eta_source)
            u_source[:, 0], u_source[:, -1] = 0, 0
            v_source = v_grid(u_source, v_previous, eta_source)
            v_source[0, :], v_source[-1, :] = 0, 0
        else:
            v_source = v_grid(u_previous, v_previous, eta_source)
            v_source[0, :], v_source[-1, :] = 0, 0
            u_source = u_grid(u_previous, v_new, eta_source)
            u_source[:, 0], u_source[:, -1] = 0, 0

        # interpolate to the eta grid
        u_prev_int, v_prev_int = u_to_eta(u_previous), v_to_eta(v_previous)
        u_s_int, v_s_int = u_to_eta(u_source), v_to_eta(v_source)

        # find the departure points
        x_dp_initial = x_mat - u_prev_int * dt
        y_dp_initial = y_mat - v_prev_int * dt

        Ld = L - dx +0.5
        # sort out dp's outside to be same as the boundary
        x_dpb1 = np.where(x_dp_initial < 0.5, 0.5, x_dp_initial)
        y_dpb1 = np.where(y_dp_initial < 0.5, 0.5, y_dp_initial)
        x_dpb = np.where(x_dpb1 < Ld, x_dpb1, Ld)
        y_dpb = np.where(y_dpb1 < Ld, y_dpb1, Ld)

        grid_pts = (y_arr, x_arr)

        # interpolating functions for the departure points
        inter_udp = RegularGridInterpolator(grid_pts, u_prev_int)
        inter_usdp = RegularGridInterpolator(grid_pts ,u_s_int)
        inter_vdp = RegularGridInterpolator(grid_pts, v_prev_int)
        inter_vsdp = RegularGridInterpolator(grid_pts, v_s_int)
        inter_etadp = RegularGridInterpolator(grid_pts, eta)
        inter_etasdp = RegularGridInterpolator(grid_pts, eta_source)

        # create a matrix of (y,x) coordinates to be inputted into inter_
        x_dp_mat = np.array([x_dpb.flatten()])
        x_dp_flat = x_dp_mat.T
        y_dp_mat = np.array([y_dpb.flatten()])
        y_dp_flat = y_dp_mat.T
        pts = np.concatenate((y_dp_flat, x_dp_flat), axis = 1)

        # find the interpolated departure points and reshape into grids
        u_dp, u_source_dp = inter_udp(pts).reshape((ny, nx)), inter_usdp(pts).reshape((ny, nx))
        v_dp, v_source_dp = inter_vdp(pts).reshape((ny, nx)), inter_vsdp(pts).reshape((ny, nx))
        eta_dp, eta_source_dp = inter_etadp(pts).reshape((ny, nx)), inter_etasdp(pts).reshape((ny, nx))


        # interpolate back to their grids
        u_dp2, v_dp2 = eta_to_u(u_dp), eta_to_v(v_dp)
        u_source_dp2, v_source_dp2 = eta_to_u(u_source_dp), eta_to_v(v_source_dp)

        # find u,v,eta using SL formula
        u_new = u_dp2 + dt*(u_source + u_source_dp2)*0.5
        v_new = v_dp2 + dt*(v_source + v_source_dp2)*0.5
        eta_new = eta_dp + dt * (eta_source + eta_source_dp) * 0.5

        # perturbation and energy calculations
        u_int, v_int = u_to_eta(u_new), v_to_eta(v_new)
        u_per, v_per, eta_per = u_ana - u_int, v_ana - v_int, eta_ana - eta_new
        energy_arr[it] = energy_fn(u_int, v_int, eta_new, rho, H, g, dx, dy)
        energy_arr_p[it] = energy_fn(u_per, v_per, eta_per, rho, H, g, dx, dy)

        # replace u, v and eta with the current time
        u, v, eta = u_new, v_new, eta_new

    final_uper, final_vper, final_etaper = u_ana - u_int, v_ana - v_int, eta_ana - eta_new

    return u,v,eta,t,energy_arr, final_uper, final_vper, final_etaper, energy_arr_p