"""
Function to calculate the energy of the model
"""
import numpy as np

def energy_fn(u ,v, eta, rho, H, g, dx, dy):
    u_squared, v_squared, eta_squared = np.square(u), np.square(v), np.square(eta)
    e_grid = 0.5*rho*(H*(u_squared + v_squared) + g*eta_squared)*dx*dy
    total_e = np.sum(e_grid)

    return total_e