"""
Numerical scheme to solve the SWEs
"""
import numpy as np
from parameters import wind_stress
from energy import energy_fn

def forward_backward(grid, initial, dimensions, setup, nt, analytic_solution):
    """
    Forward-backward numerical scheme to solve the SWEs. Calculate the perturbations
    and energy at each time point.
    Inputs:  - grid, finite difference grid
             - initial, initial conditions
             - dimensions, spatial and temporal dimensions of x,y and t
             - setup, dictionary of parameters
             - nt, number of time steps
            - analytic_solution, the analytical solution of the SWEs
    Outputs: - u, horizontal velocity after nt time steps
             - v, vertical velocity after nt time steps
             - eta, elevation after nt time steps
             - t, time array
             - energy_arr, array of energy at each time point
             - final_uper, u perturbation from analytic at final time
             - final_vper, v perturbation from analytic at final time
             - final_etaper, eta perturbation from analytic at final time
             - energy_arr_p, energy of perturbation at each time
    """
    # get parameters
    L, f_0, beta, g, gamma, rho, H, tau_0 = setup['L'], setup['f_0'], setup['beta'], \
                                            setup['g'], setup['gamma'], setup['rho'], setup['H'], setup['tau_0']

    # set dimensions
    dx, dy, nx, ny, dt = dimensions['dx'], dimensions['dy'], dimensions['nx'], dimensions['ny'], \
                             dimensions['dt']

    # set grid spacing
    u,v,eta = grid(nx, ny)

    # set initial values
    u_0, v_0, eta_0 = initial['u'], initial['v'], initial['eta']
    u.fill(u_0)
    v.fill(v_0)
    eta.fill(eta_0)

    # the actual values of x and y
    x_arr = np.arange(0, nx * dx, dx)
    y_arr = np.array([np.arange(0, ny * dy, dy)])
    y_mat = np.tile(y_arr.T, nx)

    # get wind stress
    t_x, t_y = wind_stress(tau_0, y_arr, L)
    tau_x, tau_y = np.array(t_x), np.array(t_y)

    # create u and v with no boundaries
    u_nb = np.empty([ny, nx-1])
    v_nb = np.empty([ny-1, nx])

    # functions for interpolation
    def dudx(u):
        du = np.diff(u, axis=1) # size will be (ny, nx)
        return du/dx

    def dvdy(v):
        dv = np.diff(v, axis=0) # size will be (ny, nx)
        return dv/dy

    def dndx(eta):
        dn = np.diff(eta, axis=1)   # size will be (ny, nx-1)
        return dn/dx
    def dndy(eta):
        dn = np.diff(eta, axis=0)   # size will be (ny-1, nx)
        return dn/ dy

    # functions to calculate the interpolated coriolis term
    def cor_v(f_0, b, y, v):
        frac_v = v[:-1,:-1] + v[1:,:-1] + v[:-1,1:] + v[1:,1:]
        cor = f_0 + b * (y[:, 1:] - 0.5*dy)                     # offset y as it is not on the same grid as u
        return frac_v * cor * 0.25

    def cor_u(f_0, b, y, u):
        frac_u = u[:-1,:-1] + u[1:,:-1] + u[:-1,1:] + u[1:,1:]
        cor = f_0 + b * (y[1:,:])
        return frac_u * cor * 0.25

    # function to calculate u, v and eta at time n+1
    def eta_grid(u, v, eta):
        div = dudx(u) + dvdy(v)
        return eta - (H*dt*div)

    def u_grid(u, v, eta):
        u_new = u.copy()
        m,n = np.shape(np.empty_like(u_nb))
        wind = np.tile(tau_x.T, n)
        result = cor_v(f_0,beta,y_mat,v) - g*dndx(eta) - gamma*u[:, 1:-1] + wind/(rho*H)
        u_new[:, 1:-1] = u[:, 1:-1] + dt*result
        return u_new

    def v_grid(u, v, eta):
        v_new = v.copy()
        # no wind as tau_y is zero
        result = -cor_u(f_0,beta,y_mat,u) - g*dndy(eta) - gamma*v[1:-1, :]
        v_new[1:-1, :]= v[1:-1, :] + dt*result
        return v_new

    u_new, v_new,eta_new = u.copy(), v.copy(), eta.copy()

    # time array
    t = np.arange(0, nt * dt, dt)

    # array containing energy at all times
    energy_arr = np.empty(nt)
    energy_arr_p = np.empty_like(energy_arr)

    u_ana, v_ana, eta_ana = analytic_solution

    for it in range(nt):
        eta_new = eta_grid(u,v,eta)
        if it%2 ==0:
            u_new = u_grid(u,v,eta_new)
            u_new[:, 0], u_new[:, -1] = 0, 0
            v_new = v_grid(u_new,v,eta_new)
            v_new[0,:], v_new[-1, :] = 0, 0
        else:
            v_new = v_grid(u, v, eta_new)
            v_new[0,:], v_new[-1, :] = 0, 0
            u_new = u_grid(u, v_new, eta_new)
            u_new[:, 0], u_new[:, -1] = 0, 0

        ## calculate the perturbation from resting state
        # first interpolate u and v to be on the same grid as eta
        u_int, v_int = 0.5 * (u_new[:, :-1] + u_new[:, 1:]), 0.5 * (v_new[:-1, :] + v_new[1:, :])
        u_per, v_per, eta_per = u_ana - u_int, v_ana - v_int, eta_ana - eta_new
        pers = np.array([u_per, v_per, eta_per])
        # calculate the energy at time it for steady state and perturbation
        energy_arr[it] = energy_fn(u_int, v_int, eta_new, rho, H, g, dx, dy)
        energy_arr_p[it] = energy_fn(u_per, v_per, eta_per, rho, H, g, dx, dy)

        # replace u, v and eta with the current time
        u,v,eta = u_new, v_new, eta_new

    final_uper, final_vper, final_etaper = u_ana - u_int, v_ana - v_int, eta_ana -eta_new

    return u,v,eta,t,energy_arr, final_uper, final_vper, final_etaper, energy_arr_p
