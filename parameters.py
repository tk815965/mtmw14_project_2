"""
Parameters for the SWEs
"""
import numpy as np

setup = { "L" : 1e6,        # computational domain
          "f_0" : 1e-4,     # coriolis parameter
          "beta" : 1e-11,   # beta-plane
          "g" : 10,         # gravitational acceleration
          "gamma" : 1e-6,   # linear drag coefficient
          "rho" : 1e3,      # uniform density
          "H" : 1e3,        # resting depth of the fluid (assumed constant)
          "tau_0" : 0.2     # wind stress constant
}

initial = { "u" : 0,        # initial velocity is zero
            "v" : 0,
            "eta" : 0       # initial elevation is zero
}

dimensions = {"dx" : 1e4,   # spatial step length
              "dy" : 1e4,
              "nx" : 100,   # number of steps (the steps are the number of eta points)
              "ny" : 100,
              "dt" : 70,    # temporal step length in seconds
              "nt" : np.ceil(86400/70)    # number of steps is one day
}
def wind_stress(tau_0, y, L):
    """
    Calculate the wind stress vector
    Input: tau_0, constant of wind stress
           y, spatial axis
           L, computation domain
    Output: tau, wind stress vector
    """
    tau_x = tau_0*-np.cos(np.pi*y/L)
    tau_y = tau_0*0
    return tau_x, tau_y
