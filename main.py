"""
Main script to run project 2
"""
import matplotlib.pyplot as plt
import numpy as np

from parameters import *
from solution import *
from schemes import *
from grid import *
from plots import *
from Semi_Lagrangian import *

import seaborn as sns

def task(initial, dimensions, setup):
    ### TASK C - ANALYTICAL SOLUTION
    x = np.arange(0, dimensions['nx'] * dimensions['dx'], dimensions['dx'])
    y = np.arange(0, dimensions['ny'] * dimensions['dy'], dimensions['dy'])

    analytic_sol = analytic_solution(setup, x, y)

    # plot analytic solutions
    contour_velocity(x, y, analytic_sol[0], analytic_sol[1])

    contour(x,y,analytic_sol[2])
    plt.title('Contour Plot for Elevation')
    plt.show()

    #############################################################################################################

    ### TASK D - FORWARD BACKWARDS scheme and the energy in Task E
    grid = arakawa_c

    ## ONE DAY
    u_1d, v_1d, eta_1d, t_1d, energy_1d, u_1dper, v_1dper, eta_1dper, energy_1dp = \
        forward_backward(grid, initial, dimensions, setup, int(dimensions['nt']), analytic_sol)

    xu = np.arange(0, (dimensions['nx'] + 1) * dimensions['dx'], dimensions['dx'])
    yv = np.arange(0, (dimensions['ny'] + 1) * dimensions['dy'], dimensions['dy'])
    # u versus x along the grid, closest to the southern edge of the basin
    # v versus y along the grid, closest to the western edge of the basin
    # η versus x through the middle of the gyre
    swm_plots(x, y, xu, yv, u_1d, v_1d, eta_1d)
    # a 2D contour plot showing elevation η
    contour(x,y,eta_1d)
    plt.title('Contour Plot for Elevation - FB Scheme')
    plt.show()


    ## STEADY STATE
    day = 50 # number of days to investigate steady state
    u_ss, v_ss, eta_ss, t_ss, energy_ss, u_ssper, v_ssder, eta_ssper, energy_ssp = \
        forward_backward(grid, initial, dimensions, setup, int(dimensions['nt'])*day, analytic_sol)
    swm_plots(x, y, xu, yv, u_ss, v_ss, eta_ss)
    contour(x, y, eta_ss)
    plt.title('Contour Plot for Elevation - FB Scheme at steady state')
    plt.show()

    # plot the perturbations
    contour_velocity(x, y, u_ssper, v_ssder)
    contour(x, y, eta_ssper)
    plt.title('Contour Plot for Elevation Perturbation')
    plt.show()

    # plot the energy of the steady state
    energy_plot(t_ss, energy_ss)
    plt.show()

    # plot the energy of the perturbations over time for the steady states
    energy_plot(t_ss, energy_ssp)
    plt.show()

    #############################################################################################################
    ### TASK E - energy for halved grid spacing

    ## set up new dictionary to change grid spacing and time step
    # if grid space decreases by 2 the number of step increase by 2
    new_spacing = dimensions.copy()
    new_spacing["dx"], new_spacing["dy"] = dimensions["dx"]*0.5, dimensions["dy"]*0.5
    new_spacing["nx"], new_spacing["ny"] = dimensions["nx"] * 2, dimensions["ny"] * 2

    ## after applying the CFL criterion, need dt to be less than 35.35s
    # set dt = 30 and nt = 30 days (steady state for previous time step)
    new_spacing["dt"], new_spacing["nt"] = 30, 30*np.ceil(86400/30)

    x_E = np.arange(0, new_spacing['nx'] * new_spacing['dx'], new_spacing['dx'])
    y_E = np.arange(0, new_spacing['ny'] * new_spacing['dy'], new_spacing['dy'])

    analytic_sol_E = analytic_solution(setup, x_E, y_E)

    # plot new energy
    u_E, v_E, eta_E, t_E, energy_E, u_Eper, v_Eder, eta_Eper, energy_Ep = \
        forward_backward(grid, initial, new_spacing, setup, int(new_spacing['nt']), analytic_sol_E)

    contour(x_E, y_E, eta_E)
    plt.show()
    energy_plot(t_E, energy_E)
    plt.show()

####################################################################################################################
def SL(initial, dimensions, setup):
    x = np.arange(0, dimensions['nx'] * dimensions['dx'], dimensions['dx'])
    y = np.arange(0, dimensions['ny'] * dimensions['dy'], dimensions['dy'])

    analytic_sol = analytic_solution(setup, x, y)
    grid = arakawa_c

    u_SL, v_SL, eta_SL, t_SL, energy_SL, uper_SL, vper_SL, etaper_SL, energyp_SL = \
        semi_lagrangian(grid, initial, dimensions, setup, 80, analytic_sol)

    contour(x, y, eta_SL)
    plt.title('Elevation using SL after 80 time steps')
    plt.show()

if __name__ == '__main__':
     task(initial, dimensions, setup)
     SL(initial, dimensions, setup)
