"""
This is the analytical solution of the SWEs
"""
import numpy as np


def analytic_solution(setup, x, y):
    """
    The analytic solution of the shallow water equations
    Inputs:  - setup, dictionary of parameters
             - x, array of x-values
             - y, array of y-values
    Outputs: - u, horizontal velocity
             - v, vertical velocity
             - eta, elevation
    """
    # set parameters from the setup dictionary
    L, f_0, beta, g, gamma, rho, H, tau_0 = setup['L'], setup['f_0'], setup['beta'], \
                                            setup['g'], setup['gamma'], setup['rho'], setup['H'], setup['tau_0']
    # unknown integration constant
    eta_0 = -0.11301198140396033

    # set constants
    pi, e = np.pi, np.e
    eps = gamma / (L * beta)
    a = (-1 - np.sqrt(1 + (2 * pi * eps) ** 2)) / (2 * eps)
    b = (-1 + np.sqrt(1 + (2 * pi * eps) ** 2)) / (2 * eps)
    ea, eb = e ** a, e ** b

    # initialise u, v and eta
    u = np.empty([len(x), len(y)])
    v, eta = np.empty_like(u), np.empty_like(u)

    def f_1(x):
        return pi * (1 + ((ea - 1) * eb ** x + (1 - eb) * ea ** x) / (eb - ea))

    def f_2(x):
        return ((ea - 1) * b * eb ** x + (1 - eb) * a * ea ** x) / (eb - ea)

    tau_frac = tau_0 / (pi * gamma * rho * H)

    for i in range(len(y)):
        for j in range(len(x)):
            cos_piyL = np.cos(pi * y[i] / L)
            sin_piyL = np.sin(pi * y[i] / L)

            u[i, j] = -tau_frac * f_1(x[j] / L) * cos_piyL
            v[i, j] = tau_frac * f_2(x[j] / L) * sin_piyL

            eta[i, j] = eta_0 + tau_frac * f_0 * L / g * (gamma / (f_0 * pi) * f_2(x[j] / L) * cos_piyL +
                                                          1 / pi * f_1(x[j] / L) * (sin_piyL * (1 + beta * y[i] / f_0)
                                                                                    + beta * L / (f_0 * pi) * cos_piyL))
    return u, v, eta
