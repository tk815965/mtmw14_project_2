"""
Plots for project 2
"""

import matplotlib.pyplot as plt
import numpy as np

def contour_velocity(x,y, u, v):
    # figure of 2D contour plots of velocity
    fig, (ax1, ax2) = plt.subplots(1, 2, sharex=True, sharey=True, figsize=(14, 4.8))
    fig.suptitle('Contour Plots for Velocity')
    ax1.pcolormesh(x, y, u, cmap='PRGn', shading = 'auto')
    ax1.set_title('Horizontal Velocity')
    ax2.pcolormesh(x, y, v, cmap='PRGn', shading = 'auto')
    ax2.set_title('Vertical Velocity')
    ax1.set_xlabel('Longitude (m)')
    ax1.set_ylabel('Lattitude (m)')
    plt.show()

def contour(x,y,z):
    # 2D contour plot of one variable
    plt.pcolormesh(x,y,z, cmap = 'PRGn', shading = 'auto')
    plt.xlabel('Longitude (m)')
    plt.ylabel('Lattitude (m)')
    plt.colorbar()

def swm_plots(x,y,xu,yv,u, v, z):
    # figure of 2D contour plots of velocity
    fig, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize=(20, 4.8))
    fig.suptitle('Ocean Basin PLots')
    ax1.plot(xu, u[0,:])
    ax1.set_xlabel('Longitude (m)')
    ax1.set_ylabel('Horizontal Velocity (ms-1)')
    ax1.set_title('Southern edge of the basin')
    ax2.plot(yv, v[:,0])
    ax2.set_xlabel('Lattitude (m)')
    ax2.set_ylabel('Vertical Velocity (ms-1)')
    ax2.set_title('Western Edge of the Basin')
    ny, nx = np.shape(z)
    ax3.plot(x[int(nx/4) : int(3*nx/4)], z[int(nx/4) : int(3*nx/4), 50])
    ax3.set_xlabel('Longitude (m)')
    ax3.set_ylabel('Elevation (m)')
    ax3.set_title('Middle of the Gyre')
    plt.show()

def energy_plot(t, energy):
    t_days = t/86400
    plt.plot(t_days, energy)
    plt.xlabel('Time (days)')
    plt.ylabel('Energy')
    plt.title('Total energy')

